﻿using System.Collections.Generic;

namespace dblFolders
{
    public partial class MainWindow
    {
        class DuplicateFolder
        {
            string folderName;
            List<string> allFolderPaths;

            public DuplicateFolder(string name, List<string> paths) 
            {
                folderName = name;
                allFolderPaths = paths;
            }
            public string getFolderName()
            {
                return folderName;
            }

            public List<string> getAllPaths()
            {
                return allFolderPaths;
            }

            public void deletePathFromList(string path)
            {
                allFolderPaths.Remove(path);
            }
        }
    }
}
