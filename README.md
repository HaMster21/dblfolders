# dblFolders

A simple tool that shows all identically named folders in a given root folder.

## How it works

Duplicate folder names will be shown on the left list.
Selecting an entry lists the paths to all folders on the right side.
Via the right-click context menu it is possible to copy the path, open the folder in the explorer or deleting the folder.

## Download

[dblFolders.zip](https://bitbucket.org/rosc/dblfolders/downloads/dblFolders.zip)